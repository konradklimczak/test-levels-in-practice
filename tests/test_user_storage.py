import uuid

import pytest

from models.users import User
from storage import UserStorage


@pytest.fixture
def empty_storage() -> UserStorage:
    storage = UserStorage
    storage.__users = []
    return storage


@pytest.fixture
def new_user_model() -> User:
    test_user_email = f'test.user{uuid.uuid4()}@example.com'
    test_user = User(test_user_email)
    return test_user


@pytest.fixture
def storage_with_one_user(empty_storage, new_user_model) -> UserStorage:
    empty_storage.add(new_user_model)
    return empty_storage


class TestUserStorage:
    """User Storage: """

    def test_serializing_as_list_empty(self, empty_storage):
        """Serialize storage to list when no users have been added"""
        assert empty_storage.to_list() == []

    def test_serializing_as_list_non_empty(self, storage_with_one_user, new_user_model):
        """Serialize storage to list when one user has been added"""
        assert storage_with_one_user.to_list()[0]['email'] == new_user_model.email

    def test_add_user_to_storage(self):
        pass

    def test_add_same_user_twice(self):
        pass

    def test_find_existing_user(self):
        pass

    def test_find_non_existing_user(self):
        pass

    def test_remove_existing_user(self):
        pass

    def test_remove_non_existing_user(self):
        pass
